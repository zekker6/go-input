go-input
====

[godocs]: https://godoc.org/gitlab.com/zekker6/go-input
[![pipeline status](https://gitlab.com/zekker6/go-input/badges/master/pipeline.svg)](https://gitlab.com/zekker6/go-input/commits/master)
[![coverage report](https://gitlab.com/zekker6/go-input/badges/master/coverage.svg)](https://gitlab.com/zekker6/go-input/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

`go-input` is a Go package for reading user input in console.

Here is the some good points compared with other/similar packages. It can handle `SIGINT` (`Ctrl+C`) while reading input and returns error. It allows to change IO interface as `io.Writer` and `io.Reader` so it's easy to test of your go program with this package (This package is also well-tested!). It also supports raw mode input (reading input without prompting) for multiple platform (Darwin, Linux and Windows). Not only this it allows to prompt complex input via Option struct. 

The documentation is on [GoDoc][godocs].

## Install

Use `go get` to install this package:

```bash
$ go get gitlab.com/zekker6/go-input
```

## Usage

The following is the simple example,

```golang
ui := &input.UI{
    Writer: os.Stdout,
    Reader: os.Stdin,
}

query := "What is your name?"
name, err := ui.Ask(query, &input.Options{
    Default: "tcnksm",
    Required: true,
    Loop:     true,
})
```

You can check other examples in [here](/_example).

## Contribution

1. Fork
1. Create a feature branch
1. Commit your changes
1. Rebase your local changes against the master branch
1. Run test suite with the `go test ./...` command and confirm that it passes
1. Run `gofmt -s`
1. Create new Merge Request

## Original Author
This library is a fork of [Taichi Nakashima](https://github.com/tcnksm) library [go-input](https://github.com/tcnksm/go-input)
Fork was created in order to include changes ignored by author and continue support of library if needed.
